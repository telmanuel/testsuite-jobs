def reportDir = "reports/*.xml"

def reportResult(dir) {
  junit dir
}

node {
  stage("Clone repository") {
    git "https://gitlab.com/telmanuel/testsuite.git"
  }

  stage("Test") {
    try {
      sh "mkdir -p reports"
      withCredentials([[$class: 'UsernamePasswordMultiBinding', credentialsId: "gitlab", usernameVariable: 'GITLAB_USER', passwordVariable: 'GITLAB_PW']]) {
        sh "behave --junit --junit-directory reports"
      }
    } catch(err) {
      reportResult(reportDir)
      error("${err}")
    }
  }

  stage("Report") {
    reportResult(reportDir)
  }
}
